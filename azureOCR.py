import json
import locale 
import sys
import requests
import re
import utils
from collections import defaultdict
from itertools import chain


class AzureOCR:
    azureURL = "https://westus.api.cognitive.microsoft.com/vision/v1.0/ocr?language=it&detectOrientation=true"
    totalLabel = ['totale', 'total', 'pagato','importo', 'biglietto']
    tolerance = 10


    def processRegion(self, dictionary, region):
        if 'words' in region:
            for line in region['words']:
                self.processWord(dictionary, line)
        if 'lines' in region:
            for line in region['lines']:
                self.processRegion(dictionary, line)    
            
    def processWord(self, dictionary, line):
        boundingRegion = line['boundingBox']
        boundingRegion = boundingRegion.split(',')
        text = line['text']
        self.assingTextToLine(dictionary, boundingRegion[1],text)

    def assingTextToLine(self, dictionary, pixel, text):
        found = False
        pixel = int(pixel)
        for key in iter(dictionary):
            if key - self.tolerance <= pixel and key + self.tolerance >= pixel :
                dictionary[key].append(text)
                found = True
                break
        if not found:
            dictionary[pixel] = [text]

    def findTotal(self, dictionary):
        total = list(filter(lambda ele: self.is_number(ele),chain.from_iterable(filter(lambda ele: utils.match(ele),dictionary.values()))))
        if len(total) > 0:
            return max(list(map(lambda x: float(x.replace(',','.')),total)))
        else:
            return -1

    def match(self, elements):
        match = False
        for ele in elements:
            if ele.lower() in self.totalLabel:
                match = True
                break
        return match

    def is_number(self, s):
        try:
            float(s.replace(',','.'))
            return True
        except ValueError:
            return False

    def normaliseJson(self, dictionary, json):
        regions = json['regions']
        for region in regions:
            self.processRegion(dictionary,region)

    def processJson(self, url):
        dictionary = defaultdict()
        self.normaliseJson(dictionary,self.callAzure(url))
        terms = list(chain.from_iterable(dictionary.values()))
        amount = self.findTotal(dictionary)
        dates = re.findall(r"[0-9]{2}[/\.][0-9]{2}[/\.][0-9]{2,4}", ' '.join(terms))
        response = {
            'amount': amount,
            'terms': terms,
            'numTerms': len(terms),
            'date': dates
        }
        return response

    def callAzure(self, filename):

        with open(filename, 'rb') as f:
            postData = f.read()
            response = requests.post(self.azureURL,
                                    data=postData,
                                    headers={'Content-Type': 'application/octet-stream',
                                                'Ocp-Apim-Subscription-Key': '8e259493833b481ca7f8b0da9c71adc6'})
            
            return response.json()
if __name__ == '__main__':
    url = sys.argv[1]

    #print(processJson(filename))
    azure = AzureOCR()
    result = azure.processJson(url)
    print(result)



