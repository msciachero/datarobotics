#!/usr/bin/python

# Usage: process.py <input file> <output file> [-language <Language>] [-pdf|-txt|-rtf|-docx|-xml]

import argparse
import base64
from base64 import b64encode
import getopt
import MultipartPostHandler
import os
import re
import sys
import time
import urllib2
import urllib
import xml.dom.minidom
import requests
import json
from io import BytesIO
from collections import defaultdict
from itertools import chain
import utils

class ProcessingSettings:
	Language = "English"
	OutputFormat = "docx"


class Task:
	Status = "Unknown"
	Id = None
	DownloadUrl = None
	def IsActive( self ):
		if self.Status == "InProgress" or self.Status == "Queued":
			return True
		else:
			return False

class AbbyyOnlineSdk:
	tempDir = './temp/'
	totalLabel = ['totale', 'total', 'pagato', 'contanti','importo','biglietto']
	ServerUrl = "http://cloud.ocrsdk.com/"
	# To create an application and obtain a password,
	# register at http://cloud.ocrsdk.com/Account/Register
	# More info on getting your application id and password at
	# http://ocrsdk.com/documentation/faq/#faq3
	ApplicationId = "datarobotics"
	Password = "xQDYdhelJbf9jcSBegfKLsp8"
	Proxy = None
	enableDebugging = 0

	def retrieveImage(self,url):
		resp = requests.get(url)
		return BytesIO(resp.content)

	def ProcessImage( self, filePath, settings ):
		urlParams = urllib.urlencode({
			"language" : settings.Language,
			"exportFormat" : settings.OutputFormat
			})
		requestUrl = self.ServerUrl + "processImage?" + urlParams
		
		bodyParams = { "file" : open(filePath,"rb")  }
		request = urllib2.Request( requestUrl, None, self.buildAuthInfo() )
		response = self.getOpener().open(request, bodyParams).read()
		if response.find( '<Error>' ) != -1 :
			return None
		# Any response other than HTTP 200 means error - in this case exception will be thrown

		# parse response xml and extract task ID
		task = self.DecodeResponse( response )
		return task

	def GetTaskStatus( self, task ):
		urlParams = urllib.urlencode( { "taskId" : task.Id } )
		statusUrl = self.ServerUrl + "getTaskStatus?" + urlParams
		request = urllib2.Request( statusUrl, None, self.buildAuthInfo() )
		response = self.getOpener().open( request ).read()
		task = self.DecodeResponse( response )
		return task

	def DownloadResult( self, task, outputPath ):
		getResultUrl = task.DownloadUrl
		if getResultUrl == None :
			print "No download URL found"
			return
		request = urllib2.Request( getResultUrl )
		fileResponse = self.getOpener().open( request ).read()
		resultFile = open( outputPath, "wb" )
		resultFile.write( fileResponse )

	def ProcessResult(self, task):
		getResultUrl = task.DownloadUrl
		if getResultUrl == None :
			print "No download URL found"
			return
		request = urllib2.Request( getResultUrl )
		fileResponse = self.getOpener().open( request ).read()
		lines = fileResponse.replace("\r","").split("\n")
		dictionary = defaultdict()
		counter = 0
		for line in lines:
			elements = line.split(" ")
			dictionary[counter] = list(filter(lambda x: len(x)>0, elements))
			counter+=1
		terms = list(chain.from_iterable(dictionary.values()))
		amount = self.findTotal(dictionary)
		dates = re.findall(r"[0-9]{2}[/\.][0-9]{2}[/\.][0-9]{2,4}", ' '.join(terms))
		response = {
			'amount': amount,
			'terms': terms,
			'numTerms': len(terms),
			'date': dates
		}
		return response

	def findTotal(self, dictionary):
		total = list(filter(lambda ele: self.is_number(ele),chain.from_iterable(filter(lambda ele: utils.match(ele),dictionary.values()))))
		if len(total) > 0:
			return max(list(map(lambda x: float(x.replace(',','.')),total)))
		else:
			return -1

	def match(self, elements):
		match = False
		for ele in elements:
			if ele.lower() in self.totalLabel:
				match = True
				break
		return match

	def is_number(self, s):
		try:
			float(s.replace(',','.'))
			return True
		except ValueError:
			return False

	def DecodeResponse( self, xmlResponse ):
		""" Decode xml response of the server. Return Task object """
		dom = xml.dom.minidom.parseString( xmlResponse )
		taskNode = dom.getElementsByTagName( "task" )[0]
		task = Task()
		task.Id = taskNode.getAttribute( "id" )
		task.Status = taskNode.getAttribute( "status" )
		if task.Status == "Completed":
			task.DownloadUrl = taskNode.getAttribute( "resultUrl" )
		return task


	def buildAuthInfo( self ):
		return { "Authorization" : "Basic %s" % base64.b64encode( "%s:%s" % (self.ApplicationId, self.Password) ) }

	def getOpener( self ):
		if self.Proxy == None:
			self.opener = urllib2.build_opener( MultipartPostHandler.MultipartPostHandler,
			urllib2.HTTPHandler(debuglevel=self.enableDebugging))
		else:
			self.opener = urllib2.build_opener( 
				self.Proxy, 
				MultipartPostHandler.MultipartPostHandler,
				urllib2.HTTPHandler(debuglevel=self.enableDebugging))
		return self.opener


