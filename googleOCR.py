import json
import locale 
import sys
import requests
import re
from io import BytesIO
from collections import defaultdict
from itertools import chain
from base64 import b64encode
from os import makedirs
from os import path
from os.path import join, basename, exists
import jellyfish
import utils

class GoogleOCR:
    
    tolerance = 10 
    ENDPOINT_URL = 'https://vision.googleapis.com/v1/images:annotate'
    RESULTS_DIR = 'jsons'
    api_key = 'AIzaSyAu6emlxl_QegaBK5G7yrTkQ5P8As1Jwfs'

    def make_image_data_list(self, filename):
        """
        image_filenames is a list of filename strings
        Returns a list of dicts formatted as the Vision API
            needs them to be
        """
        img_requests = []
        with open(filename, "rb") as f:
            ctxt = b64encode(f.read()).decode()
            img_requests.append({
                        'image': {'content': ctxt},
                        'features': [{
                            'type': 'TEXT_DETECTION',
                            'maxResults': 1
                        }]
            })
        return img_requests
    
    def make_image_data(self, filename):
        """Returns the image data lists as bytes"""
        imgdict = self.make_image_data_list(filename)
        return json.dumps({"requests": imgdict }).encode()
    
    
    def request_ocr(self, api_key, image_filenames):
        response = requests.post(self.ENDPOINT_URL,
                                data=self.make_image_data(image_filenames),
                                params={'key': self.api_key},
                                headers={'Content-Type': 'application/json'})
        return response
    
    
    def processResponse(self,dictionary, response):
        for annotation in response['textAnnotations']:
            if 'description' in annotation and 'locale' not in annotation:
                description = annotation['description']
                vertices = annotation['boundingPoly']['vertices']
                averageY = 0
                for coord in vertices:
                    averageY += coord['y']
                averageY = int(averageY/len(vertices))
                self.assingTextToLine(dictionary,averageY, description)

    def assingTextToLine(self,dictionary, pixel, text):
        found = False
        pixel = int(pixel)
        for key in iter(dictionary):
            if key - self.tolerance <= pixel and key + self.tolerance >= pixel :
                dictionary[key].append(text)
                found = True
                break
        if not found:
            dictionary[pixel] = [text]            
        
            
    def processWord(self,dictionary, description):
        counter = 0
        for lines in description.split('\n'):
            if counter not in dictionary:
                dictionary[counter] = lines.split(' ')
            else:
                dictionary[counter] = dictionary[counter] + lines.split(' ')
            counter = counter+1

    def findTotal(self,dictionary):
        total = list(filter(lambda ele: self.is_number(ele),chain.from_iterable(filter(lambda ele: utils.match(ele),dictionary.values()))))
        if len(total) > 0:
            return max(list(map(lambda x: float(x.replace(',','.')),total)))
        else:
            return -1

    

    def is_number(self,s):
        try:
            float(s.replace(',','.'))
            return True
        except ValueError:
            return False

    def normaliseJson(self,dictionary, json):
        responses = json['responses']
        for response in responses:
            self.processResponse(dictionary,response)

    def processJson(self,filename):
        dictionary = defaultdict()
        self.normaliseJson(dictionary,self.request_ocr(self.api_key, filename).json())
        terms = list(chain.from_iterable(dictionary.values()))
        print(dictionary)
        amount = self.findTotal(dictionary)
        dates = re.findall(r"[0-9]{2}[/\.][0-9]{2}[/\.][0-9]{2,4}", ' '.join(terms))
        response = {
            'amount': amount,
            'terms': terms,
            'numTerms': len(terms),
            'date': dates
        }
        return response








