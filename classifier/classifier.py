import io
import jellyfish
from collections import defaultdict
comment_char = '#'

class Classifier:
    dictionary = defaultdict()
    fileName = './classifier.properties'
    
    def __init__(self):
        self.load()

    def load(self):
        with open(self.fileName, "r") as f:
            for line in f:
                l = line.strip()
                if l and not l.startswith(comment_char):
                    key_value = l.split('=')
                    key = key_value[0].strip()
                    value = ''.join(key_value[1:]).strip().strip('"') 
                    self.dictionary[key] = value
        

    def classify(self, words):
        candidateTerms = []
        for word in words:
            lword = word.lower().strip()
            
            
            for key in iter(self.dictionary.keys()):
                try:
                    ukey = key
                    if isinstance(key, bytes):
                        ukey = unicode(key)
                    uword = lword
                    if isinstance(lword, bytes):
                        uword = unicode(lword)
                    result = 1000
                    if len(lword) <= 3:
                        if lword == key:
                            result = 0
                    else:
                        result = jellyfish.levenshtein_distance(ukey, uword)
                    #print(uword + "-" + ukey+ ":"+ str(result))
                    if result < 3:
                        candidateTerms.append({'term' : self.dictionary[key], 'distance': result})
                     #   print(candidateTerms)
                except:
                    continue
        if len(candidateTerms) > 0:
            candidateTerms = sorted(candidateTerms, key= lambda term: term['distance'])
            #print(candidateTerms)
            #print("Term {0} - {1} distance {2}".format(lword, candidateTerms[0]['term'],candidateTerms[0]['distance']))
            return candidateTerms[0]['term']

if __name__ == "__main__":
    print(Classifier().classify([u'panificio']))
