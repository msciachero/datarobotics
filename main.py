from classifier.classifier import Classifier
import json 
from azureOCR import AzureOCR
from googleOCR import GoogleOCR
from abby.process import AbbyProcessor
import sys
import os
import re
import requests
from io import BytesIO
from wand.image import Image as wandImage
import validators
import web
from os import listdir
from os.path import isfile, join
from collections import defaultdict
import math
from dateutil import parser
import datetime

tempDir = './temp/'

urls = (
    '/process/(.*)', 'processImage'
)

minDate = datetime.datetime(2016,1,1)

def convertToJpeg(filename):
    if filename[-3:].lower() == "pdf" : 
        jpg = filename[:-3] + "jpg"
        img = wandImage(filename=filename)
        img.save(filename=jpg)
        return jpg
    else:
        return filename

def retrieveImage(url):
		resp = requests.get(url)
		return BytesIO(resp.content)

def saveFile(url, filename):
    if validators.url(url):
        if not os.path.exists(tempDir):
            os.makedirs(tempDir)
        tempFile =tempDir+filename 
        with open(tempFile, "wb") as f:
            f.write(retrieveImage(url).read())
        return convertToJpeg(tempFile)
    else:
        return convertToJpeg(url)


def rank(candidate, classification):
    result = 0
    if classification != None:
        result += 10
    if candidate.get('amount') > -1:
        result += 20
    aDate = candidate.get('date')
    if len(aDate) > 0:
        result += 20/len(aDate)
    result = math.log(candidate['numTerms'])*result
    return result


def process(image_filenames):
    filename = image_filenames.split('/')[-1].split('#')[0].split('?')[0]
    tempFile = saveFile(image_filenames, filename)
    outcome = defaultdict()
    googleOcr = GoogleOCR()
    outcome['google'] = googleOcr.processJson( tempFile)
    print("Response Google %s" % outcome['google'])
    azureOcr = AzureOCR()
    
    outcome['azure'] = azureOcr.processJson( tempFile)
    #print("Response Azure %s" % outcome['azure'])
    outcome['abbyy'] =  AbbyProcessor().recognizeFile( tempFile,  'Italian', 'txt' )
    print("Response Abbyy %s" % outcome['abbyy'])
    classifier = Classifier()
    result = outcome.get('abbyy').get('terms')
    outcome['abbyyClassification'] = classifier.classify(result)
    result = outcome.get('google').get('terms')
    outcome['googleClassification'] = classifier.classify(result)
    result = outcome.get('azure').get('terms')
    outcome['azureClassification'] = classifier.classify(result)
    #print("Google: {0} Azure: {1} Abby: {2}".format(outcome['googleClassification'] , outcome['azureClassification']  , outcome['abbyyClassification']))
    selected = outcome.get('google')
    classification = outcome.get('googleClassification')
    selRank = rank(selected,classification)
    selectedSource = 'google'
    for name in ['google','abbyy','azure']:
        candidate = outcome.get(name)
        candidateClass = outcome.get(name+'Classification')
        candidateRank = rank(candidate, candidateClass)
        print("Source: {0} Rank: {1} PreviousRank {2}".format(name, candidateRank, selRank))
        if(candidateRank > selRank ):
            selected = candidate
            classification = candidateClass
            selRank = candidateRank 
            selectedSource = name
    
    dataList = selected.get('date')
    datas = 'N/A'
    if len(dataList) > 0 :
        dataList = list(filter(lambda x: x != None, map(lambda x: convertDate(x),dataList)))
        if len(dataList) > 0:
            datetimes = max(dataList)
            if datetimes > minDate:
                datas = datetimes.strftime('%d/%m/%Y')
        #datas =",".join(dataList)
    amount = selected.get('amount')
    if amount == -1 :
        amount = 'N/A'
    return'{0};{1};{2};{3}\n'.format(filename, classification, amount, datas)

def convertDate(datas):
    try:
        return parser.parse(datas, fuzzy=True,dayfirst=True)
    except ValueError:
        return None

class processImage:
    def GET(self, image):
        print("receive image {0}".format(image))
        mypath = "./pdf/"+image
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        results = []
        counter = 1
        for aFile in onlyfiles:
            print('Processing file {0} of {1}'.format(counter,len(onlyfiles)))
            results.append(process(mypath+"/"+aFile))
            counter += 1
        web.header('Content-Type','text/txt')
        response =  "".join(results)
        print(response)
        return response


app = web.application(urls, globals())

if __name__ == "__main__":
    app.run()