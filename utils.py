import jellyfish

totalLabel = [u'totale', u'total', u'pagato', u'contanti',u'importo',u'biglietto']

def match(elements):
    match = False
    for ele in elements:
        if len(ele) < 4:
            # too short, not useful
            continue
        for compare in totalLabel:
            try:
                uword = ele.lower()
                if isinstance(uword, bytes):
                    uword = unicode(uword)
                result = jellyfish.levenshtein_distance(compare, uword)
                #print(uword + "-" + compare+ ":"+ str(result))
                if result < 3:
                    return True
            except:
                #do nothing
                match=False
    return False

if __name__ == "__main__":
    print(match(['totali', 'mark']))
    print(match(['totalias', 'mark']))
    print(match(['TOTALE', '\xe2\x82\xac', '6,44']))